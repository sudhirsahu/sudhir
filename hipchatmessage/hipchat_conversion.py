# -*- coding: utf-8 -*-
"""
This program takes a user message as string type and returns a JSON string.
The following requirements should match in its contents.
Special content to look for includes:
1. @mentions - A way to mention a user. Always starts with an '@' and ends when
 Hitting a non-word character.
 (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
2. Emoticons - Predefined emoticons which are alphanumeric strings, no longer
than 15 characters, contained in parenthesis.
 You can assume that anything matching this format is an emoticon.
 (https://www.hipchat.com/emoticons)
3. Links - Any URLs contained in the message, along with the page's title.

Example:
    Input: "@bob @john (success) such a cool feature;
    https://twitter.com/jdorfman/status/430511497475670016"
    Return (string):
    {
      "mentions": [
        "bob",
        "john"
      ],
      "emoticons": [
        "success"
      ],
      "links": [
        {
          "url": "https://twitter.com/jdorfman/status/430511497475670016",
          "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
        }
      ]
    }
"""
# import standard python modules
import re
import json
import pprint
import urllib2
# import custom emoticons that we support for message conversion
from custom_emoticons_config import predefined_emoticons

class HipChatOperations(object):
    """This class with support all operations for a chipchat message"""
    
    def __init__(self, pinged_messages):
        """
        Args:
            pinged_messages (str): pinged_messages parameter a str message.           
        """
        try:
            self.pinged_messages = str(pinged_messages) 
        except ValueError, error:
            print str(error)
            print "Opps..This program takes message as a str"
    
    def findall_mentions(self, pinged_messages):
        """
        Args:
            pinged_messages (str): pinged_messages parameter should be str
            query (str): query parameter should be key to search from hipchat dict
        Return:
            filtered_mentions_list(list): if found mention return as list of mentions
        Mentions - Always starts with an '@' and ends non-chat char.                  
        """
        try:
            # filtered_mentions_list (list): stored for mentions content
            filtered_mentions_list = []
            # search all contents starts with @ and 
                                  #ends with non-char from message
            for chat in re.split('[^@A-Za-z0-9_]', pinged_messages):    
                mentions_search_pattern = '^(@1?)([a-zA-Z]+)'
                if re.match(mentions_search_pattern, chat):
                    filtered_mentions_list.append(chat[1:]) 
            return filtered_mentions_list
        except Exception, error:
            print str(error)
            print "Opps something went wrong in findall_mentions"
        
    def findall_emoticons(self, pinged_messages):
        """
        Args:
            pinged_messages (str): pinged_messages parameter should be a chat message.
            query (str): query parameter should be key to search from hipchat dict
        Return:
            filtered_emoticons_list(list): search emoticon return as list of emoticons
        Emoticons - alphanumeric str, no longer than 15 char, contained in parenthesis                 
        """
        try:
            # filtered_emoticons_list(list): stored for emoticons content
            filtered_emoticons_list = []
            # search contents in parenthesis, alphanumeric, max char 17
            search_obj = re.search(r'\(([a-zA-Z0-9]{,15})\).*', pinged_messages)
            search_values = search_obj.group()
            filtered_emoticons = search_values.split(' ')
            for emoticon in filtered_emoticons:     
                # match contents contained in parenthesis 
                if re.match(r"\((.*)\)", emoticon, re.M|re.I):
                    if len(emoticon) <= 17:
                        if emoticon in predefined_emoticons:
                            filtered_emoticons_list.append(emoticon[1:-1])
            return filtered_emoticons_list
        except Exception, error:
            print str(error)
            print "Opps something went wrong in findall_emoticons"
    def findall_url_links(self, pinged_messages):
        """
        Args:
            pinged_messages (str): pinged_messages parameter should be a chat message.
            query (str): query parameter should be key to search from hipchat dict
        Return:
            self.filtered_links_list(list): if found link return as list of dict contains url & title        
        Links - Any URLs contained in the message, along with the page's title.            
        """  
        try:
            # self.filtered_links_list (list): stored for links content
            filtered_links_list = []
            # url_links(dict) - keys & values of url and title.
            # match all contents starts with http or https as a web link or url
            filtered_urls = re.findall(
                            r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
                            pinged_messages)
            if filtered_urls:
                for url in filtered_urls:                      
                    try:
                        html_title = None
                        html_response = urllib2.urlopen(url)
                        html_contents = html_response.read()
                        title_begin = html_contents.find('<title>')
                        title_end = html_contents.find('</title>')
                        html_title = html_contents[
                                                  title_begin+
                                                  len('<title>'):title_end
                                                  ].strip()  
                        filtered_links_list.append({'url':url,
                                      'title':html_title})
                        
                    except Exception, error:
                        print str(error)
                        print "URL is not correct or host is not responding"
                        html_title = ''
                        filtered_links_list.append({'url':url,
                                      'title':html_title})
                    
                          
            return filtered_links_list
        except Exception, error:
            print str(error)
            print "Opps something went wrong in findall_url_links"
        
    def filter_hipchat_messages(self, query_param=None):
        """"
        In this method, the user is asked an input as str type and
        Conversion string message to hipchat message format return json str.
        Args:           
            query (list): query param should be key to search from hipchat dict
            e.g=['Mentions', 'Emoticons', 'Links']
        Return:
            response_message (json str): response_message is a json str format
            This method will return response_message if query matched 
        Raise:
            Raise ValueError if input parameter is not a valid str type
        """ 
        try:
            # self.pinged_messages (str): use pinged_messages init mehtod param
            # filtered_result_message (dict): matched mentions,emoticons & urls 
            self.filtered_message = {}
            # define query list to serve query response message
            self.predefined_query = ['Mentions', 'Emoticons', 'Links']
            # dict of mentions, emoticons and links are filtered from chat      
            if self.findall_mentions(self.pinged_messages):
                if "Mentions" in query_param:
                    self.filtered_message.update({
                                                "mentions" :
                                                self.findall_mentions
                                                (self.pinged_messages)
                                                })
            if self.findall_emoticons(self.pinged_messages):
                if "Emoticons" in query_param:
                    self.filtered_message.update({
                                                "emoticons" :
                                                self.findall_emoticons
                                                (self.pinged_messages)
                                                })
            if self.findall_url_links(self.pinged_messages):
                if "Links" in query_param:
                    self.filtered_message.update({
                                                "links" :
                                                self.findall_url_links
                                                (self.pinged_messages)
                                                })
            pprint.pprint(self.filtered_message)   
            # return filtered message as json str object based on query
            self.response_message = json.dumps(self.filtered_message)
            return self.response_message
        except Exception, error:
            print str(error)
            print "Opps something went wrong in filter_hipchat_messages"
    
if __name__ == "__main__":
    try:
        # input_message = input("Please enter a hipchat message as str type")
        user_ping = "@bob+@sam (cake) @all (success) a cool; http://google.com"
        message_obj = HipChatOperations(user_ping)
        # pass query parameter as list of query items
        message_obj.filter_hipchat_messages(query_param=
                                            ['Mentions', 'Emoticons', 'Links'])
    except ValueError, error:
        print str(error)
        print("Opps! Something went wrong... Try again")
    
        
